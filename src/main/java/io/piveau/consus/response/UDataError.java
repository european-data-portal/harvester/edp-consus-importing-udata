package io.piveau.consus.response;

import io.vertx.core.json.JsonObject;

public class UDataError extends HttpError<JsonObject> {

	public UDataError(JsonObject error) {
		super(error);
	}

	@Override
	public String getType() {
		return error.getString("__type");
	}

	@Override
	public String getMessage() {
		return error.getString("message");
	}
	
}
