package io.piveau.consus.response;

public abstract class HttpResult<T> {

    protected final T result;

    protected HttpResult(T result) {
        this.result = result;
    }

    protected abstract T getContent();

}
